# Matrix Echo Bot

This is a simple example showing the basics of receiving and sending messages using [Matrix Rust SDK](https://github.com/matrix-org/matrix-rust-sdk).
When run, this bot will echo back any messages sent to it.

# Todo
* Accept invites. Currently you will have to log in via a client and accept the chat invite to get started.