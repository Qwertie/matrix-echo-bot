extern crate dotenv;

use dotenv::dotenv;
use std::env;
use std::convert::TryFrom;
use matrix_sdk::{
    Client, SyncSettings, Result, uuid::Uuid, room::Room,
    ruma::{
        UserId,
        events::{SyncMessageEvent,
            room::message::{MessageEventContent, MessageType},
            AnyMessageEventContent
        }
    },
};

#[tokio::main]
async fn main() -> Result<()> {
    dotenv().ok();
    let username = env::var("MATRIX_USERNAME").expect("Set env MATRIX_USERNAME");
    let password = env::var("MATRIX_PASSWORD").expect("Set env MATRIX_PASSWORD");
    let user_id = UserId::try_from(username)?;
    let client = Client::new_from_user_id(user_id.clone()).await?;

    client.login(
        user_id.localpart(),
        &password,
        None,
        None
    ).await?;

    client
        .register_event_handler(echo_text_events)
        .await;

    client.sync(SyncSettings::default()).await;
    Ok(())
}

async fn echo_text_events(ev: SyncMessageEvent<MessageEventContent>, room: Room, client: Client) -> Result<()> {
    println!("Received a message {:?} \n\n {:?}", ev.content.msgtype, room.room_id());
    // Ignore our own messages or we would end up in an infinite loop.
    if ev.sender == client.user_id().await.unwrap() {
        return Ok(());
    }

    let txn_id = Uuid::new_v4();
    let room_id = room.room_id();
    let content: AnyMessageEventContent;

    // Respond to text messages only. Ignore others.
    match ev.content.msgtype {
        MessageType::Text(text) => {
            content = AnyMessageEventContent::RoomMessage(
                MessageEventContent::text_plain("Echo: ".to_string() + &text.body),
            );
        },
        _ => {
            return Ok(());
        }
    }

    client.room_send(room_id, content, Some(txn_id)).await.unwrap();
    Ok(())
}